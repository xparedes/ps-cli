#!/usr/bin/env node
const yargs = require('yargs')
const sdk = require('./../lib/sdk')
const genTransactions = require('./../lib/functions/gen-transactions')

yargs
  .command(
    'make:card:payment',
    'make card payments',
    {
      amount: {
        describe: 'payment amount',
        type: 'string',
        demandOption: true,
      },
      currency: {
        describe: 'payment currency',
        type: 'string',
        default: 'USD',
        demandOption: true,
      },
      vanityName: {
        describe: 'customer vanity name',
        type: 'string',
        demandOption: true,
      },
      payer: {
        describe: 'payer info. [json string]',
        type: 'string',
      },
      card: {
        describe: 'card to use for the payment [json string]',
        type: 'string',
      },
    },
    sdk.makeCardPayment
  )
  .command(
    'make:bank:payment',
    'make bank payments',
    {
      amount: {
        describe: 'payment amount',
        type: 'string',
        demandOption: true,
      },
      currency: {
        describe: 'payment currency',
        type: 'string',
        default: 'USD',
      },
      customerId: {
        describe: 'customer id',
        type: 'string',
        demandOption: true,
      },
      payer: {
        describe: 'payer info. [json string]',
        type: 'string',
      },
      bank: {
        describe: 'bank to use for the payment [json string]',
        type: 'string',
      },
    },
    sdk.makeBankPayment
  )
  .command(
    'make:customer',
    'create a customer',
    {
      customerFile: {
        describe: 'path to the json file that contain the customer info.',
        type: 'string',
      },
      name: {
        describe: 'customer name. only if not set in the customer info file.',
        type: 'string',
      },
      planKey: {
        describe:
          'the plan key to use. only if not set in the customer info file.',
        type: 'string',
      },
      facilitatorId: {
        describe: 'facilitator id',
        type: 'string',
      },
    },
    sdk.createCustomer
  )
  .command(
    'make:withdrawal',
    'create a withdrawal for a given customer',
    {
      amount: {
        describe: 'withdrawal amount',
        type: 'string',
      },
      currency: {
        describe: 'payment currency',
        type: 'string',
        default: 'USD',
      },
      customerId: {
        describe: 'customer id',
        type: 'string',
        demandOption: true,
      },
      bankId: {
        describe: 'destination bank id',
        type: 'string',
      },
    },
    sdk.makeWithdrawal
  )
  .command(
    'make:dispute',
    'create a payment dispute',
    {
      amount: {
        describe: 'dispute amount',
        type: 'string',
        demandOption: true,
      },
      settlementAmount: {
        describe: 'dispute settlement amount',
        type: 'string',
      },
      currency: {
        describe: 'payment currency',
        type: 'string',
        default: 'USD',
      },
      settlementCurrency: {
        describe: 'payment currency',
        type: 'string',
      },
      description: {
        describe: 'dispute description',
        type: 'string',
      },
      paymentId: {
        describe: 'the id of disputed payment',
        type: 'string',
      },
      customerId: {
        describe: 'customer id',
        type: 'string',
        demandOption: true,
      },
      amountWon: {
        describe: 'dispute won amount',
        type: 'string',
      },
    },
    sdk.makeDispute
  )
  .command(
    'make:refund',
    'make payment refund',
    {
      amount: {
        describe: 'refund amount',
        type: 'string',
        demandOption: true,
      },
      currency: {
        describe: 'payment currency',
        type: 'string',
        default: 'USD',
      },
      description: {
        describe: 'refund description',
        type: 'string',
      },
      paymentId: {
        describe: 'the id of payment to refund',
        type: 'string',
      },
      customerId: {
        describe: 'customer id',
        type: 'string',
        demandOption: true,
      },
    },
    sdk.makeRefund
  )
  .command(
    'make:adjustment',
    'create an adjustment',
    {
      amount: {
        describe: 'adjustment amount',
        type: 'string',
        demandOption: true,
      },
      currency: {
        describe: 'adjustment currency',
        type: 'string',
        default: 'USD',
      },
      customerId: {
        describe: 'customer id',
        type: 'string',
        demandOption: true,
      },
      adjustmentType: {
        describe: 'adjustment type',
        type: 'string',
        demandOption: true,
      },
      escrowName: {
        describe: 'escrow name',
        type: 'string',
        demandOption: true,
      },
      description: {
        describe: 'refund description',
        type: 'string',
      },
    },
    sdk.makeAdjustment
  )
  .command(
    'gen:transactions',
    'generate transactions for testing',
    {
      numOfCustomer: {
        describe: 'number of customer to create',
        type: 'number',
      },
      transactionsPerCustomer: {
        describe: 'number of transactions to generate per customer',
        type: 'number',
        demandOption: true,
      },
      paymentsOnly: {
        describe: 'if set to true, it will generate only payments',
        type: 'boolean',
        default: false,
      },
      customerId: {
        describe:
          'default customer id. only if we do not have to generate customer',
        type: 'string',
      },
      planKey: {
        describe: 'plan to use for generated customer',
        type: 'string',
      },
    },
    genTransactions
  )
  .help().argv
