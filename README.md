## Getting Started

Run `npm install` to install dependencies. You can install the cli globally for easy use by running `npm install -g .` while you're in the `ps-cli` root directory

## Commands

You can run `ps-cli` from any directory

### Available commands

```
    make:card:payment  make card payments
    make:bank:payment  make bank payments
    make:customer      create a customer
    make:withdrawal    create a withdrawal for a given customer
    make:dispute       create a payment dispute
    make:refund        make payment refund
    make:adjustment    create an adjustment
    gen:transactions   generate transactions for testing
```

Run `ps-cli [COMMAND] --help` to see the available options

#### make:card:payment

e.g.:

```
ps-cli make:card:payment --amount=500 --vanityName="[CUSTOMER_VANITY_NAME]"
```

#### make:bank:payment

e.g.:

```
ps-cli make:bank:payment --amount=500 --vanityName="[CUSTOMER_VANITY_NAME]"
```
