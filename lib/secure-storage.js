const _ = require('lodash')
const fs = require('fs')
const logError = require('./helpers/log-error')

const SECURE_FILE_DIR = `${process.env.HOME}/secure.json`

const get = (path) => {
  let secure = {}
  try {
    secure = require(SECURE_FILE_DIR)
  } catch (error) {
    logError('request-get-secure', {
      error: error,
      ref: 'b89f30279c969f26c13130e9600ec1df',
    })
  }
  let returns = secure

  if (path) returns = _.get(secure, path)

  return returns
}

const set = async (key, value) => {
  try {
    let secure = get() || {}
    secure = _.merge(secure, { [key]: value })

    const secureData = JSON.stringify(secure, null, 4)
    fs.writeFileSync(SECURE_FILE_DIR, secureData)
  } catch (error) {
    logError('request-set-secure-failed', {
      ref: '0bcf12a5d861f49b63e62992ced7916a',
      error: error,
    })
  }
}

const deleteAll = async () => {
  fs.writeFileSync(SECURE_FILE_DIR, '{}')
}

const setIsSecure = async () => await set('is_secure', true)

const setCustomerId = async (customerId) => await set('customer_id', customerId)

const setPublishableKey = async (publishableKey) =>
  await set('publishable_key', publishableKey)

module.exports = {
  get,
  set,
  delete: deleteAll,
  setIsSecure,
  setCustomerId,
  setPublishableKey,
}
