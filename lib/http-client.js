const _ = require('lodash')
const axios = require('axios').default
const https = require('https')
const logError = require('./helpers/log-error')
const log = require('./helpers/log')
const { endpoints } = require('./constants')
const secureStorage = require('./secure-storage')

const BASE_API_URL = 'https://localhost:3001/api/v3'

const httpsAgent = new https.Agent({ rejectUnauthorized: false })
const httpClient = axios.create({
  baseURL: BASE_API_URL,
})

httpClient.interceptors.request.use(
  (config) => {
    const excluded = config.url.includes(endpoints.LOGIN)
    const {
      publishable_key = null,
      token_type = null,
      access_token = null,
      customer_id = null,
      is_secure = false,
    } = !excluded ? secureStorage.get() : {}

    config.headers['Accept'] = 'application/json'

    if (publishable_key && config.url.includes(endpoints.PUBLIC_PAYMENTS))
      config.headers['X-PUBLISHABLE-KEY'] = publishable_key
    if (customer_id) config.headers['X-CUSTOMER-ID'] = customer_id
    if (token_type && access_token && is_secure)
      config.headers['Authorization'] = `${token_type} ${access_token}`

    config.httpsAgent = httpsAgent

    log('api-call-request', {
      method: config.method,
      url: config.url,
      params: config.params,
      data: config.data,
      customerId: customer_id,
      publishableKey: publishable_key,
    })

    return config
  },
  (error) => {
    logError('request-interceptor', {
      error,
      ref: 'c0cebd6fc9843071efec6e7a4694c88c',
    })
    return Promise.reject({ error })
  }
)

httpClient.interceptors.response.use(
  (response) => {
    log('api-call-success')
    return Promise.resolve(response.data || {})
  },
  (error) => {
    logError('api-call-request', {
      error: {
        code: _.get(error, 'response.status'),
        message: _.get(error, 'response.data.message'),
      },
      data: _.get(error, 'response.data'),
      ref: '1fb0188faeaccadc61c2107eaaf904d0',
    })
    return Promise.reject(error)
  }
)

module.exports = httpClient
