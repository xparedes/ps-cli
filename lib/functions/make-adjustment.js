const isRequired = require('../helpers/is-required')
const auth = require('../auth')
const logError = require('../helpers/log-error')
const httpClient = require('../http-client')
const { endpoints } = require('../constants')
const log = require('../helpers/log')

module.exports = async function ({
  customerId,
  amount,
  currency = 'USD',
  adjustmentType,
  escrowName,
  description,
}) {
  isRequired(
    { customerId, amount, adjustmentType, escrowName },
    'df8b056dac92b0f8d2e6d1f64e3166ce',
    ['customerId', 'amount', 'adjustmentType', 'escrowName']
  )

  await auth.setAccess({ customerId })

  let response = {}

  const payload = {
    amount,
    currency,
    adjustmentType,
    escrowName,
    description: description || 'console adjustment',
  }

  try {
    response = await httpClient.post(endpoints.ADJUSTMENTS_CREATE, payload)

    log('response-create-adjustment', response)
  } catch (error) {
    logError('request-make-adjustment', {
      ref: '6410f22d10a8b0c42590905b97912727',
      error,
      data: {},
    })
  }

  return response
}
