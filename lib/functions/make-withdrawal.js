const log = require('../helpers/log')
const logError = require('../helpers/log-error')
const auth = require('../auth')
const { endpoints } = require('../constants')
const isRequired = require('../helpers/is-required')
const httpClient = require('../http-client')

module.exports = async function ({
  amount,
  customerId,
  bankId,
  currency = 'USD',
}) {
  isRequired(customerId, 'ac5ea5155b69c8ef22df566310bd7609', 'customerId')
  // set auth
  await auth.setAccess({ customerId })
  let response = {}
  try {
    // get default bankId from customer
    if (!bankId) {
      const customerEndpoint = endpoints.CUSTOMERS_GET.replace(
        ':id',
        customerId
      )
      const customer = await httpClient.get(customerEndpoint)
      bankId = customer.defaultBank.id
    }
    let payload = { bankId, currency, description: 'console withdrawal' }

    if (amount) payload.amount = amount
    // create withdrawal
    response = await httpClient.post(endpoints.WITHDRAWALS, payload)
  } catch (error) {
    logError('request-make-withdrawal', {
      error,
      ref: '5d15382b7097ed5eaa3e457e339659f0',
      data: { amount, customerId, bankId, currency },
    })
  }
  log('request-make-withdrawal-success', {
    id: response.id,
    amount: response.amount,
    currency: response.currency,
    destinationBankId: response.bankId,
    accountKey: response.accountKey,
  })
  return response
}
