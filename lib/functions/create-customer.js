require('dotenv').config()
const fs = require('fs')
const _ = require('lodash')
const log = require('../helpers/log')
const logError = require('../helpers/log-error')
const auth = require('../auth')
const { endpoints } = require('../constants')
const isRequired = require('../helpers/is-required')
const httpClient = require('../http-client')
const secureStorage = require('../secure-storage')

module.exports = async function ({
  customerFile,
  facilitatorId,
  name,
  planKey,
}) {
  facilitatorId =
    process.env.FACILITATOR_ID || facilitatorId || '8e0db2deccfb4b218bf9085b'
  isRequired({ facilitatorId, planKey }, '9ca3651bf474ce21b72bd9e325e7b402', [
    'facilitatorId',
    'planKey',
  ])
  let customer = {}
  if (_.isEmpty(customerFile)) {
    const customerTemplate = require('./../../data/customer.json')
    const strTemplate = JSON.stringify(customerTemplate)
      .replace(/\{\{customerName\}\}/g, name)
      .replace(/\{\{planKey\}\}/g, planKey)
    customer = JSON.parse(strTemplate)
  } else {
    try {
      const customerData = await fs.readFileSync(customerFile)
      customer = JSON.parse(customerData)
    } catch (error) {
      logError('get-customer-file', {
        error,
        ref: 'e75353b52dd8c8d3d99d10b9ebd9a9d6',
        data: {},
      })
    }
  }
  // set auth
  await auth.setAccess({ customerId: facilitatorId })
  let _customer = {}

  try {
    _customer = await httpClient.post(endpoints.CUSTOMERS_ACCOUNT, customer)
    // verify bank
    const bankEndpoint = endpoints.VERIFY_BANK.replace(
      ':id',
      _customer.account.defaultBank.id
    )
    await httpClient.put(bankEndpoint, { amounts: ['32', '45'] })
    // refresh customer
    await secureStorage.setCustomerId(_customer.account.id)
    _customer = await httpClient.post(endpoints.CUSTOMERS_REFRESH)
  } catch (error) {
    logError('request-create-customer', {
      error,
      data: {},
      ref: '435299e330584ab228b0546c88267bef',
    })
  }
  log('request-create-customer-success', {
    id: _customer.id,
    name: _customer.name,
    vanityName: _customer.vanityName,
    planId: _customer.planId,
    planName: _customer.planName,
    planStatus: _customer.planStatus,
  })
  return _customer
}
