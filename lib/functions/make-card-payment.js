const log = require('../helpers/log')
const logError = require('../helpers/log-error')
const { endpoints } = require('../constants')
const isRequired = require('../helpers/is-required')
const logPaymentResponse = require('../helpers/log-payment-response')
const httpClient = require('../http-client')
const secureStorage = require('../secure-storage')

module.exports = async function ({
  amount,
  currency = 'USD',
  vanityName,
  card,
  payer,
}) {
  card = card || require('./../../data/cards.json').USD.card
  payer = payer || require('./../../data/cards.json').USD.payer

  isRequired({ amount, vanityName }, '9e39177fa5ecd3b9810ab8558b6be511', [
    'amount',
    'vanityName',
  ])
  // set auth
  await secureStorage.delete()
  await secureStorage.setPublishableKey(vanityName)
  // init payment
  let initData = {}
  try {
    log('request-init-payment')
    initData = await httpClient.post(endpoints.SPLIT_FEES, {
      subtotal: amount,
      currency: currency,
      card: card,
    })
    log('request-init-payment-success')
  } catch (error) {
    logError('request-init-payment', {
      error,
      data: {},
      ref: '0e763cf39824cbe98f33d5911bcfc679',
    })
  }
  const { cardPayments = {} } = initData

  const mainPayload = {
    amount: cardPayments.payerTotal || amount,
    currency: currency,
    payer: payer,
    card: card,
    feeSplit: {
      subtotal: cardPayments.subtotal,
      feeSplitType: cardPayments.feeSplitType,
      customRate: cardPayments.customRate,
      customFlat: cardPayments.customFlat,
    },
    recaptchaToken: 'fake-token',
  }

  log('request-make-card-payment', mainPayload)
  let response = {}

  try {
    response = await httpClient.post(endpoints.PUBLIC_PAYMENTS, mainPayload)
  } catch (error) {
    logError('request-make-card-payment', {
      error: error,
      data: mainPayload,
      ref: 'e298a8de43218e5a883160c0d78b1964',
    })
  }

  logPaymentResponse(response)
  return response
}
