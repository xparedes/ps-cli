require('dotenv').config()
const log = require('../helpers/log')
const logError = require('../helpers/log-error')
const auth = require('../auth')
const { endpoints } = require('../constants')
const isRequired = require('../helpers/is-required')
const logPaymentResponse = require('../helpers/log-payment-response')
const httpClient = require('../http-client')

module.exports = async function ({
  amount,
  currency = 'USD',
  customerId,
  bank,
  payer,
}) {
  bank = bank || require('./../../data/bank.json').USD
  payer = payer || require('./../../data/payer.json')
  customerId = customerId || process.env.CUSTOMER_ID

  isRequired({ amount, customerId }, '208f5d49ebbc90e87733ec15e4d93c06', [
    'amount',
    'customerId',
  ])
  // Set auth for the request
  await auth.setAccess({ customerId })

  const data = { amount, currency, bank, payer }

  log('request-make-bank-payment', { ...data, customerId })
  let response = {}

  try {
    response = await httpClient.post(endpoints.SECURE_PAYMENTS, data)
  } catch (error) {
    logError('request-make-bank-payment', {
      error,
      data: { ...data, customerId },
      ref: '0f7916925200633a4d3866cccc60a7c8',
    })
  }

  logPaymentResponse(response)

  return response
}
