const _ = require('lodash')
const log = require('../helpers/log')
const logError = require('../helpers/log-error')
const { endpoints } = require('../constants')
const isRequired = require('../helpers/is-required')
const httpClient = require('../http-client')
const auth = require('./../auth')

module.exports = async function ({
  amount,
  currency = 'USD',
  description,
  customerId,
  paymentId,
}) {
  isRequired(
    { amount, customerId, paymentId },
    'de403c48a207920aa4b707ae05a6b8a4',
    ['amount', 'customerId', 'paymentId']
  )

  await auth.setAccess({ customerId })

  let response = {}
  try {
    const payload = {
      amount,
      currency,
      description: description || 'console refund',
    }
    response = await httpClient.post(
      endpoints.REFUNDS_CREATE.replace(':id', paymentId),
      payload
    )
    log('response-create-refund', _.omit(response, ['resource']))
  } catch (error) {
    logError('request-create-refund', {
      ref: '2bd2ee2aa68bed776f47e8687ea7857d',
      error,
      data: { amount, currency, description, customerId, paymentId },
    })
  }

  return response
}
