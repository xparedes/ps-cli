const _ = require('lodash')
const isRequired = require('../helpers/is-required')
const auth = require('../auth')
const logError = require('../helpers/log-error')
const httpClient = require('../http-client')
const { endpoints } = require('../constants')
const log = require('../helpers/log')

module.exports = async function ({
  amount,
  settlementAmount,
  currency = 'USD',
  settlementCurrency,
  description,
  paymentId,
  customerId,
  amountWon,
}) {
  isRequired(
    { amount, currency, paymentId, customerId },
    '83d0b764c37bf32a8c6075ec40731c82',
    ['amount', 'currency', 'paymentId', 'customerId']
  )

  if (currency === 'USD') {
    settlementAmount = amount
    settlementCurrency = currency
  }

  await auth.setAccess({ customerId })

  let response = {}

  try {
    const payload = {
      amount,
      settlementAmount,
      currency,
      settlementCurrency,
      description: description || 'console dispute',
      paymentId,
    }
    // create dispute
    const dispute = await httpClient.post(endpoints.DISPUTES_CREATE, payload)
    log('response-create-dispute', _.omit(dispute, ['resource', 'fees']))
    // change status to won
    response = await httpClient.post(
      endpoints.DISPUTES_CHANGE_STATE.replace(':id', dispute.id),
      {
        amountWon: amountWon ? amountWon : amount,
        newStatus: 'won',
      }
    )
    log(
      'response-change-dispute-status',
      _.omit(response, ['resource', 'fees'])
    )
  } catch (error) {
    logError('request-create-dispute', {
      ref: '063804cd4316a902f6220358cf6055d3',
      error: error,
      data: { amount, currency, description, paymentId, customerId },
    })
  }
  return response
}
