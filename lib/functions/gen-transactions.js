const sdk = require('../sdk')
const { faker } = require('@faker-js/faker')
const log = require('../helpers/log')
const httpClient = require('../http-client')
const { endpoints } = require('../constants')
const secureStorage = require('../secure-storage')
const auth = require('./../auth')

const makePayments = async (
  { numOfPayments, withReversals = false, secured, customer },
  callback
) => {
  let totalAmount = 0
  let totalFees = 0
  let payments = []

  for (let i = 0; i < numOfPayments; i++) {
    let _amount = faker.finance.amount(100, 500)
    let customerIdKey = secured ? 'customerId' : 'vanityName'
    let customerIdValue = customer[customerIdKey]

    let _payment = await callback({
      amount: _amount,
      [customerIdKey]: customerIdValue,
    })
    // 3 percent of card payments will be refunded/disputed
    if (i <= Math.ceil((i * 3) / 100) && !withReversals && !secured)
      payments.push(_payment)

    totalAmount += parseFloat(_amount)
    totalFees += (_payment.fees || []).reduce((total, _fee) => {
      total = total + parseFloat(_fee.amount)
      return total
    }, 0)
  }

  return { totalAmount, payments, totalFees }
}

const makeDisputes = async (payments = [], customerId) => {
  let totalAmount = 0
  let totalWonAmount = 0
  let totalFees = 0

  for (let i = 0; i < payments.length; i++) {
    let payment = payments[i]
    const disputeWonAmounts = [
      parseFloat(payment.amount),
      Math.ceil(parseFloat(payment.amount) / 2),
    ]
    let _amountKey = i % 2
    let amountWon = disputeWonAmounts[_amountKey]

    let dispute = await sdk.makeDispute({
      amount: payment.amount,
      customerId: customerId,
      paymentId: payment.id,
      amountWon: amountWon,
    })

    totalAmount += parseFloat(dispute.amount)
    totalWonAmount += amountWon
    totalFees += (dispute.fees || []).reduce((total, _fee) => {
      total += _fee.amount
      return total
    }, 0)
  }

  return { totalAmount, totalWonAmount, totalFees }
}

const makeRefunds = async (payments, customerId) => {
  let totalAmount = 0

  for (let i = 0; i < payments.length; i++) {
    let payment = payments[i]
    let refundAmount = Math.ceil(parseFloat(payment.amount) / 3)
    await sdk.makeRefund({
      customerId: customerId,
      paymentId: payment.id,
      amount: refundAmount,
    })

    totalAmount += refundAmount
  }

  return { totalAmount }
}

module.exports = async function ({
  numOfCustomer,
  transactionsPerCustomer,
  paymentsOnly = true,
  customerId,
  planKey = 'cache_split',
}) {
  await secureStorage.delete()
  const customerIds = []

  if (customerId) {
    await auth.setAccess({ customerId })
    let _customer = await httpClient(
      endpoints.CUSTOMERS_GET.replace(':id', customerId)
    )
    customerIds.push({
      customerId: _customer.id,
      vanityName: _customer.vanityName,
    })
  }

  if (!customerId) {
    for (let i = 0; i < numOfCustomer; i++) {
      let _customer = await sdk.createCustomer({
        name: faker.name.findName(),
        planKey,
      })
      customerIds.push({
        customerId: _customer.id,
        vanityName: _customer.vanityName,
      })
    }
  }

  const metrics = []

  for (const _customerId of customerIds) {
    let _metrics = {
      totalPaymentsAmount: 0,
      totalRefundsAmount: 0,
      totalDisputesAmount: 0,
      totalDisputesWonAmount: 0,
      totalPaymentFees: 0,
      totalTransfersAmount: 0,
      totalDisputeFessAmount: 0,
    }

    let paymentsToBeReversed = []
    const numOfCardPayments = Math.ceil(parseInt(transactionsPerCustomer) / 2)
    const numOfBakPayments =
      parseInt(transactionsPerCustomer) - numOfCardPayments
    // card payments
    const cardPayments = await makePayments(
      {
        numOfPayments: numOfCardPayments,
        withReversals: paymentsOnly,
        secured: false,
        customer: _customerId,
      },
      sdk.makeCardPayment
    )
    // bank payments
    const bankPayments = await makePayments(
      {
        numOfPayments: numOfBakPayments,
        withReversals: paymentsOnly,
        secured: true,
        customer: _customerId,
      },
      sdk.makeBankPayment
    )

    // payments to be reversed if any
    paymentsToBeReversed.push(
      ...(cardPayments.payments || []),
      ...(bankPayments.payments || [])
    )

    _metrics.totalPaymentsAmount =
      cardPayments.totalAmount + bankPayments.totalAmount
    _metrics.totalPaymentFees = cardPayments.totalFees + bankPayments.totalFees
    // create reversal
    if (paymentsToBeReversed.length) {
      await secureStorage.delete()
      let paymentsToBeRefunded = []
      let paymentsToBeDisputed = []

      if (paymentsToBeReversed.length >= 2) {
        let half = Math.ceil(paymentsToBeReversed.length / 2)
        paymentsToBeRefunded = paymentsToBeReversed.slice(0, half)
        paymentsToBeDisputed = paymentsToBeReversed.splice(-half)
      } else {
        // if only one payment, we're going to create only a refund
        paymentsToBeRefunded.push(...(paymentsToBeReversed || []))
      }

      // create disputes
      const disputes = await makeDisputes(
        paymentsToBeDisputed,
        _customerId.customerId
      )
      _metrics.totalDisputesAmount += disputes.totalAmount
      _metrics.totalDisputesWonAmount += disputes.totalWonAmount
      _metrics.totalDisputeFessAmount += disputes.totalFees
      // create refunds
      const refunds = await makeRefunds(
        paymentsToBeRefunded,
        _customerId.customerId
      )
      _metrics.totalRefundsAmount += refunds.totalAmount
    }

    const netDisputes =
      _metrics.totalDisputesAmount - _metrics.totalDisputesWonAmount
    const netOut =
      netDisputes +
      _metrics.totalRefundsAmount +
      _metrics.totalPaymentFees +
      _metrics.totalDisputeFessAmount
    const netIn = _metrics.totalPaymentsAmount + _metrics.totalDisputesWonAmount
    const netBalances = netIn - netOut

    _metrics = {
      ..._metrics,
      netDisputes,
      netOut,
      netIn,
      netBalances,
    }

    _metrics = Object.keys(_metrics).reduce((_m, key) => {
      _m[key] = parseFloat(_metrics[key]).toFixed(2)
      return _m
    }, {})
    log(
      `transactions-generated-for-customerId-${_customerId.customerId}`,
      _metrics
    )

    metrics.push({ customerId, ..._metrics })
  }

  return metrics
}
