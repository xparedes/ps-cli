const colors = require('colors')

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red',
})

module.exports = function (logKey, data, logType = 'info') {
  const strData = ` >> ${JSON.stringify(data)} <<`[logType]

  console.log(`[${logKey}]`.debug, strData)
}
