const log = require('./log')

module.exports = function (key, error) {
  log(
    `Error -> ${key}`,
    {
      ref: error.ref,
      errorCode: error.error.code || 'UNKNOWN_ERROR',
      message: error.error.message || `An unknown error occurred on [${key}].`,
      data: error.data || {},
    },
    'error'
  )
}
