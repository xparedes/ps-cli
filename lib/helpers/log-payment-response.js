const log = require('../helpers/log')

module.exports = function (data) {
  log('request-payment-success', {
    paymentId: data.id,
    amount: data.amount,
    currency: data.currency,
    payerName: (data.payer || {}).name,
    payerId: (data.payer || {}).id,
    sourceLast4: (data.source || {}).last4,
    sourceId: data.sourceId,
    sourceType: data.sourceType,
    ownerId: data.ownerId,
  })
}
