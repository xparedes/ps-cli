const _ = require('lodash')
const logError = require('../helpers/log-error')

module.exports = function (data, ref, keys = []) {
  const _logError = (ref, data, key) => {
    logError('missing-requirements', {
      error: {
        code: 'MissingRequirement',
        message: `[${key}] is required.`,
      },
      data: data,
      ref: ref,
    })
  }

  if (_.isObject(data) && _.isArray(keys)) {
    Object.keys(data).forEach((key) => {
      if (
        !keys.includes(key) ||
        (keys.includes(key) && _.isEmpty(`${data[key]}`))
      ) {
        _logError(ref, data, key)
        throw new Error()
      }
    })
  } else if (_.isObject(data) && _.isString(keys)) {
    const value = _.get(data, keys)
    if (!value) {
      _logError(ref, data, keys)
    }
  } else {
    if (_.isEmpty(data)) _logError(ref, { [keys.toString()]: data }, keys)
  }
}
