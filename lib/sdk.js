const createCustomer = require('./functions/create-customer')
const makeAdjustment = require('./functions/make-adjustment')
const makeBankPayment = require('./functions/make-bank-payment')
const makeCardPayment = require('./functions/make-card-payment')
const makeDispute = require('./functions/make-dispute')
const makePaymentRefund = require('./functions/make-payment-refund')
const makeWithdrawal = require('./functions/make-withdrawal')

module.exports = {
  makeBankPayment: makeBankPayment,
  makeCardPayment: makeCardPayment,
  makeWithdrawal: makeWithdrawal,
  createCustomer: createCustomer,
  makeDispute: makeDispute,
  makeRefund: makePaymentRefund,
  makeAdjustment: makeAdjustment,
}
