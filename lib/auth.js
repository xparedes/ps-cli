const httpClient = require('./http-client')
const log = require('./helpers/log')
const logError = require('./helpers/log-error')
const secureStorage = require('./secure-storage')
const { endpoints } = require('./constants')
const isRequired = require('./helpers/is-required')

const authenticate = async (email, password) => {
  email = process.env.PS_EMAIL || 'developers@paystand.com'
  password = process.env.PS_PASSWORD || 'Paystand!23'

  log('request-authenticate', { email })

  try {
    await secureStorage.set('is_secure', false)

    const response = await httpClient.post(endpoints.LOGIN, { email, password })
    // save accessToken for feature use
    Object.keys(response).map(
      async (key) => await secureStorage.set(key, response[key])
    )
    log('request-authenticate-success')
  } catch (error) {
    logError('request-authentication-failed', {
      error: error,
      ref: '59336bc7401f2d7c7a8e025adce22dbb',
    })
  }
}

const setAccess = async ({ customerId, publishableKey }) => {
  isRequired(customerId, '6e0736dbeb825a2878b8744ea97213af', 'customerId')
  log('request-set-api-access')

  await secureStorage.delete()

  await authenticate()
  await secureStorage.setIsSecure()
  await secureStorage.setCustomerId(customerId)

  if (publishableKey) await secureStorage.setPublishableKey(publishableKey)

  log('request-set-api-access-success')
}

const logout = async () => await secureStorage.delete()

module.exports = {
  authenticate,
  setAccess,
  logout,
}
